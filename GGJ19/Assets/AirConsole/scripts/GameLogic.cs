﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;

public class GameLogic : MonoBehaviour {

	public GameObject logo;
	public TvController tvController;
	public Text logWindow;
	private bool turnLeft;
	private bool turnRight;

	private float highScore = 0;
	public Text highScoreDisplay;

#if !DISABLE_AIRCONSOLE
	void Awake () {
		// register events
		AirConsole.instance.onReady += OnReady;
		AirConsole.instance.onMessage += OnMessage;
		AirConsole.instance.onConnect += OnConnect;
		AirConsole.instance.onDisconnect += OnDisconnect;
		AirConsole.instance.onGameEnd += OnGameEnd;
		AirConsole.instance.onHighScores += OnHighScores;
		AirConsole.instance.onHighScoreStored += OnHighScoreStored;
		logWindow.text = "Connecting... \n \n";
	}

	void OnReady (string code) {
		//Log to on-screen Console
		logWindow.text = "ExampleBasic: AirConsole is ready! \n \n";

		//Mark Buttons as Interactable as soon as AirConsole is ready
		Button[] allButtons = (Button[])GameObject.FindObjectsOfType ((typeof(Button)));
		foreach (Button button in allButtons) {
			button.interactable = true;
		}
	}

	void OnMessage (int from, JToken data) {
		//Log to on-screen Console
		logWindow.text = logWindow.text.Insert (0, "Incoming message from device: " + from + ": " + data.ToString () + " \n \n");
		
		// Rotate the AirConsole Logo to the right
		if ((string)data == "left") {
			turnLeft = true;
			turnRight = false;
		}

		// Rotate the AirConsole Logo to the right
		if ((string)data == "right") {
			turnLeft = false;
			turnRight = true;
		}

		// Stop rotating the AirConsole Logo
		//'stop' is sent when a button on the controller is released
		if ((string)data == "stop") {
			turnLeft = false;
			turnRight = false;
		}

		//Show an Ad
		if ((string)data == "show_ad") {
			AirConsole.instance.ShowAd ();
		}
	}

	void OnConnect (int device_id) {
		//Log to on-screen Console
		logWindow.text = logWindow.text.Insert (0, "Device: " + device_id + " connected. \n \n");
	}

	void OnDisconnect (int device_id) {
		//Log to on-screen Console
		logWindow.text = logWindow.text.Insert (0, "Device: " + device_id + " disconnected. \n \n");
	}

	void OnGameEnd () {
		Debug.Log ("OnGameEnd is called");
		Camera.main.enabled = false;
		Time.timeScale = 0.0f;
	}

	void OnHighScores (JToken highscores) {
		//Log to on-screen Console
		logWindow.text = logWindow.text.Insert (0, "On High Scores " + highscores + " \n \n");
		//logWindow.text = logWindow.text.Insert (0, "Converted Highscores: " + HighScoreHelper.ConvertHighScoresToTables(highscores).ToString() + " \n \n");
	}

	void OnHighScoreStored (JToken highscore) {
		//Log to on-screen Console
		if (highscore == null) {
			logWindow.text = logWindow.text.Insert (0, "On High Scores Stored (null)\n \n");
		} else {
			logWindow.text = logWindow.text.Insert (0, "On High Scores Stored " + highscore + "\n \n");
		}		
	}

	void Update () {
		//If any controller is pressing a 'Rotate' button, rotate the AirConsole Logo in the scene
		if (turnLeft) {
			this.logo.transform.Rotate (0, 0, 2);
		
		} else if (turnRight) {
			this.logo.transform.Rotate (0, 0, -2);
		}
	}

	public void ScramblePlayers (int id) {
		//Say Hi to the first controller in the GetControllerDeviceIds List.

		//We cannot assume that the first controller's device ID is '1', because device 1 
		//might have left and now the first controller in the list has a different ID.
		//Never hardcode device IDs!
		int idOfFirstController = AirConsole.instance.GetControllerDeviceIds () [id];

		AirConsole.instance.Message (idOfFirstController, "Hey there, first controller!");

		//Log to on-screen Console
		logWindow.text = logWindow.text.Insert (0, "Sent a message to first Controller \n \n");
	}

	public void BroadcastMessageToAllDevices () {
		AirConsole.instance.Broadcast ("Hey everyone!");
		logWindow.text = logWindow.text.Insert (0, "Broadcast a message. \n \n");
	}

	public void SetActivePlayers () {
		//Set the currently connected devices as the active players (assigning them a player number)
		AirConsole.instance.SetActivePlayers ();

		string activePlayerIds = "";
		foreach (int id in AirConsole.instance.GetActivePlayerDeviceIds) {
			activePlayerIds += id + "\n";
		}

		//Log to on-screen Console
		logWindow.text = logWindow.text.Insert (0, "Active Players were set to:\n" + activePlayerIds + " \n \n");
	}

	public void IncreaseScore () {
		//increase current score and show on ui
		highScore += 1;
		highScoreDisplay.text = "Current Score: " + highScore;
	}

	public void ResetScore () {
		//reset current score and show on ui
		highScore = 0;
		highScoreDisplay.text = "Current Score: " + highScore;
	}

	public void RequestHighScores () {
		List <string> ranks = new List<string> ();
		ranks.Add ("world");
		AirConsole.instance.RequestHighScores ("Basic Example", "v1.0", null, ranks, 5, 3);
	}

	public void StoreHighScore () {
		JObject testData = new JObject();
		testData.Add ("test", "data");
		AirConsole.instance.StoreHighScore ("Basic Example", "v1.0", highScore, AirConsole.instance.GetUID(AirConsole.instance.GetMasterControllerDeviceId()), testData);
	}

	public void StorePersistentData () {
		//Store test data for the master controller
		JObject testData = new JObject();
		testData.Add ("test", "data");
		AirConsole.instance.StorePersistentData("custom_data", testData, AirConsole.instance.GetUID(AirConsole.instance.GetMasterControllerDeviceId()));
	}

	public void RequestPersistentData () {
		List<string> connectedUids = new List<string> ();
		List<int> deviceIds = AirConsole.instance.GetControllerDeviceIds();
		
		for (int i = 0; i < deviceIds.Count; i++) {
			connectedUids.Add (AirConsole.instance.GetUID(deviceIds[i]));
		}
		AirConsole.instance.RequestPersistentData (connectedUids);
	}

	void OnDestroy () {

		// unregister events
		if (AirConsole.instance != null) {
			AirConsole.instance.onReady -= OnReady;
			AirConsole.instance.onMessage -= OnMessage;
			AirConsole.instance.onConnect -= OnConnect;
			AirConsole.instance.onDisconnect -= OnDisconnect;
			AirConsole.instance.onGameEnd -= OnGameEnd;
			AirConsole.instance.onHighScores -= OnHighScores;
			AirConsole.instance.onHighScoreStored -= OnHighScoreStored;
		}
	}
#endif
}

