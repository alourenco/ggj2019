﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip mainMenuTrack;
    public AudioClip inGameTrack;

    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void stop()
    {
        _audioSource.Stop();
    }

    public void playMainMenuTrack()
    {
        _audioSource.clip = mainMenuTrack;
        _audioSource.Play();
    }

    public void playInGameTrack()
    {
        _audioSource.clip = inGameTrack;
        _audioSource.Play();
    }
}
