﻿
public class ProtoConstants
{
    // Controller to Screen
    public static string ready = "ready";

    public static string left =    "left";
    public static string right =   "right";
 
    public static string white =   "white";
    public static string black =   "black";
    public static string red =     "red";
    public static string green =   "green";
    public static string blue =    "blue";
    public static string yellow =  "yellow";

    // Screen to controller
    public static string starting = "starting";

    public static string scramble = "scramble";
}
