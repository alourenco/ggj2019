﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;

public class GameManager : MonoBehaviour
{
    public TvController tvController;
    public PlayerManager playerManager;
    public HUDManager hudManager;
    public AudioManager audioManager;
    public SoundFXManager soundFXManager;

    public int scoreIfCorrect = 10;
    public int scoreIfWrong = -2;
    public int secondsToStartRound = 10;
    public int secondsToStartFirstRound = 5;
    public int secondsPerRound = 30;
    public int winsToWin = 3;

    public GameObject menu;
    public GameObject connected1;
    public GameObject connected2;
    public GameObject connected3;
    public GameObject connected4;

    public GameObject ready1;
    public GameObject ready2;
    public GameObject ready3;
    public GameObject ready4;

    private float _elapsedTime = 0.0F;
    private int _startElapsedSeconds = 0;
    private int _roundElapsedSeconds = 0;
    private bool _isCountingDownToStart = false;
    private int _currentRound = 1;
    private int _secondsToStartCurrentRound;

    void Awake()
    {
        AirConsole.instance.onConnect += onConnect;
        AirConsole.instance.onDisconnect += onDisconnect;
        AirConsole.instance.onMessage += onMessage;

        _secondsToStartCurrentRound = secondsToStartFirstRound;
    }

    private void Start()
    {
        audioManager.playMainMenuTrack();
    }

    void Update()
    {
        if (_isCountingDownToStart) {
            updateStartCounter();
        } else if (tvController.isTvOn()) {
            updateRound();
        }
    }

    void updateStartCounter()
    {
        if (_elapsedTime >= 1.0F) {
            _startElapsedSeconds++;
            _elapsedTime = 0.0F;
            hudManager.updateGameStartCounter(_secondsToStartCurrentRound - _startElapsedSeconds);
        }

        if (_startElapsedSeconds >= _secondsToStartCurrentRound) {
            _isCountingDownToStart = false;
            _startElapsedSeconds = 0;
            hudManager.hideGameStartCounter();
            startGame();
            _secondsToStartCurrentRound = secondsToStartRound;
            return;
        }

        _elapsedTime += Time.deltaTime;
    }

    void updateRound()
    {
        if (_elapsedTime >= 1.0F) {
            _elapsedTime = 0.0F;
            _roundElapsedSeconds++;
            updateRoundTimer();
        }

        _elapsedTime += Time.deltaTime;

        if (_roundElapsedSeconds >= secondsPerRound) {
            var winnerInfo = playerManager.checkForWinner();
            winnerInfo.addWin();
            endRound(winnerInfo);
        }
    }

    void updateRoundTimer()
    {
        hudManager.updateRoundTimer(secondsPerRound - _roundElapsedSeconds);
    }

    void endRound(PlayerManager.PlayerInfo winnerInfo)
    {
        stopGame();

        hudManager.updateWins(playerManager.getPlayerSpawnedPosition(winnerInfo), winnerInfo.getWins());
        if (winnerInfo.getWins() >= winsToWin) {
            hudManager.showEndMenu(winnerInfo.getPlayerName());
        } else {
            hudManager.updateAllScores(playerManager);
            hudManager.showResultHUD(winnerInfo.getPlayerName());
            startGameCountdown();
            _currentRound++;
            hudManager.updateCurrentRound(_currentRound);
        } 
    }

    void onConnect(int deviceId)
    {
        Debug.Log("Player connected with deviceId:" + deviceId);
        playerManager.registerPlayer(deviceId);
        enableOrDisableConnected(playerManager.getPlayerSpawnedPosition(playerManager.getPlayerInfo(deviceId)), true);
    }

    void onDisconnect(int deviceId)
    {
        Debug.Log("Player disconnected with deviceId: " + deviceId);
        enableOrDisableConnected(playerManager.getPlayerSpawnedPosition(playerManager.getPlayerInfo(deviceId)), false);
        playerManager.unregisterPlayer(deviceId);
    }

    void onMessage(int deviceId, JToken data)
    {
        string action = (string)data["action"];
        if (tvController.isTvOn() && !action.Equals("")) {
            onInGameMessage(deviceId, data);

        } else if (action.Equals("ready")) {
            playerManager.getPlayerInfo(deviceId).setIsReady(true);
            enableOrDisableReady(playerManager.getPlayerSpawnedPosition(playerManager.getPlayerInfo(deviceId)), true);
            if (allPlayersReady()) {
                sendMessageToAllPlayers();
                startGameCountdown();
            }

        }

        string name = (string)data["name"];
        if (!name.Equals("")) {
            playerManager.getPlayerInfo(deviceId).setPlayerName(name);
            updateScoreName(deviceId);
        }

    }

    void sendMessageToAllPlayers() {
        var orderedPlayerArray = playerManager.getOrderedPlayersArray();
        for (int i = 0; i < orderedPlayerArray.Length; i++) {
            if (orderedPlayerArray[i] != null) {
                AirConsole.instance.Message(orderedPlayerArray[i].deviceId, "allplayersready");
            }
        }   
    }

    void onInGameMessage(int deviceId, JToken data)
    {
        string action = (string)data["action"];

        if (action.Equals(ProtoConstants.left)) {
            int victimDeviceId = playerManager.hitPlayerLeftOfDeviceId(deviceId);
            scrambleController(victimDeviceId);
            soundFXManager.playHitSound();
        } else if (action.Equals(ProtoConstants.right)) {
            int victimDeviceId = playerManager.hitPlayerRightOfDeviceId(deviceId);
            scrambleController(victimDeviceId);
            soundFXManager.playHitSound();
        } else if (action.Equals(tvController.getCurrentColor())) {
            updateScore(deviceId, scoreIfCorrect);
        } else if (!action.Equals(tvController.getCurrentColor())) {
            updateScore(deviceId, scoreIfWrong);
        }
    }

    void updateScore(int deviceId, int scoreDelta)
    {
        var playerInfo = playerManager.getPlayerInfo(deviceId);
        playerInfo.addScore(scoreDelta);
        updateScoreUI(playerInfo, deviceId);
    }

    void updateScoreUI(PlayerManager.PlayerInfo playerInfo, int deviceId)
    {
        int index = playerManager.getPlayerSpawnedPosition(playerInfo);
        hudManager.updateScore(index, playerManager.getPlayerInfo(deviceId));
    }

    void scrambleController(int deviceId)
    {
        var message = new { action = ProtoConstants.scramble };
        AirConsole.instance.Message(deviceId, message);
    }

    public void startGame()
    {
        hudManager.updateRoundTimer(secondsPerRound);
        tvController.turnOn();
    }

    public void startGameCountdown()
    {
        if (_currentRound == 1)
        {
            audioManager.playInGameTrack();
        }

        _isCountingDownToStart = true;
        hudManager.closeMainMenu();
        hudManager.updateGameStartCounter(_secondsToStartCurrentRound);
    }

    public void stopGame()
    {
        tvController.turnOff();
        _elapsedTime = 0.0F;
        _roundElapsedSeconds = 0;
    }

    private void OnDestroy()
    {
        if (AirConsole.instance != null) {
            AirConsole.instance.onMessage -= onMessage;
        }
    }

    private void enableOrDisableConnected(int deviceId, bool enableOrDisable) {
        switch (deviceId) {
            case 0:
                connected1.SetActive(enableOrDisable);
                break;
            case 1:
                connected2.SetActive(enableOrDisable);
                break;
            case 2:
                connected3.SetActive(enableOrDisable);
                break;
            case 3:
                connected4.SetActive(enableOrDisable);
                break;
        }
    }

    private void enableOrDisableReady(int deviceId, bool enableOrDisable) {
        switch (deviceId) {
            case 0:
                ready1.SetActive(enableOrDisable);
                break;
            case 1:
                ready2.SetActive(enableOrDisable);
                break;
            case 2:
                ready3.SetActive(enableOrDisable);
                break;
            case 3:
                ready4.SetActive(enableOrDisable);
                break;
        }
    }

    private bool allPlayersReady() {

        if (playerManager.getConnectedPlayers() >= 2) {
            for (int i = 0; i < playerManager._orderedPlayersArray.Length; i++) {
                if (playerManager._orderedPlayersArray[i] != null && playerManager._orderedPlayersArray[i].getIsReady() == false) {
                    return false;
                }
            }
            return true;
        }
        return false;        
    }

    private void updateScoreName(int deviceId){
        var playerInfo = playerManager.getPlayerInfo(deviceId);
        int index = playerManager.getPlayerSpawnedPosition(playerInfo);
        hudManager.enableScore(index, playerInfo);
    }

}
