﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public class PlayerInfo
    {
        public GameObject playerObject;
        public int deviceId = -1;
        private int _score = 0;
        private int _wins = 0;
        private bool isReady = false;
        private string playerName = "";

        public void addScore(int scoreDelta)
        {
            _score += scoreDelta;

            if (_score < 0) {
                _score = 0;
            }
        }

        public int getScore()
        {
            return _score;
        }

        public void resetScore()
        {
            _score = 0;
        }

        public void addWin()
        {
            _wins++;
        }

        public int getWins()
        {
            return _wins;
        }

        public void setIsReady(bool ready) {
            isReady = ready;
        }

        public bool getIsReady() {
            return isReady;
        }

        public void setPlayerName(string playerName) {
            this.playerName = playerName;
        }

        public string getPlayerName() {
            return playerName;
        }
    }

    public GameObject playerObject;
    public Transform[] spawners;

    public PlayerInfo[] _orderedPlayersArray;
    private PlayerInfo[] _playersInSpawnersArray;

    private int connectedPlayers = 0;

    void Start()
    {
        _orderedPlayersArray = new PlayerInfo[] {null, null, null, null};
        _playersInSpawnersArray = new PlayerInfo[] { null, null, null, null };
    }

    public void registerPlayer(int deviceId)
    {
        PlayerInfo playerInfo = new PlayerInfo();
        playerInfo.deviceId = deviceId;

        for (int i = 0; i < spawners.Length; i++)
        {
            if (!isPlayerInSpawner(i))
            {
                playerInfo.playerObject = Instantiate(playerObject, spawners[i].position, spawners[i].rotation);
                _orderedPlayersArray[mapSpawnerToPlayerIndex(i)] = playerInfo;
                _playersInSpawnersArray[i] = playerInfo;
                connectedPlayers++;
                break;
            }
        }

    }

    public bool isPlayerInSpawner(int index)
    {
        return _playersInSpawnersArray[index] != null;
    }

    public int getNextFreePosition()
    {
        for (int i = 0; i < _orderedPlayersArray.Length; i++) {
            if (_orderedPlayersArray[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public void unregisterPlayer(int deviceId)
    {
        for (int i = 0; i < _orderedPlayersArray.Length; i++) {
            if (_orderedPlayersArray[i].deviceId == deviceId) {
                _orderedPlayersArray[i] = null;
                connectedPlayers--;
            }

            if (_playersInSpawnersArray[i].deviceId == deviceId) {
                Destroy(_playersInSpawnersArray[i].playerObject);
                _playersInSpawnersArray[i] = null;
                return;
            }
        }
    }

    public int mapSpawnerToPlayerIndex(int spawnIndex)
    {
        switch (spawnIndex)
        {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 0;
            case 3:
                return 3;
            default:
                return -1;
        }
    }

    public int hitPlayerLeftOfDeviceId(int deviceId)
    {
        return hitPlayerWithRelativePosition(deviceId, -1);
    }

    public int hitPlayerRightOfDeviceId(int deviceId)
    {
        return hitPlayerWithRelativePosition(deviceId, 1);
    }

    private int hitPlayerWithRelativePosition(int deviceId, int deltaIndex)
    {
        PlayerInfo playerInfo = getPlayerInfo(deviceId);
        int playerIndex = getPlayerOrderedPosition(playerInfo);

        PlayerInfo victimInfo = getNextNeighbor(deltaIndex, playerIndex);
        animatePlayerHit(playerInfo, deltaIndex);

        return victimInfo.deviceId;
    }

    private PlayerInfo getNextNeighbor(int deltaIndex, int startingIndex)
    {
        PlayerInfo neighboor = null;
        int currentIndex = startingIndex + deltaIndex;
        while (neighboor == null) {
            int index;
            if (currentIndex < 0) {
                index = currentIndex + _orderedPlayersArray.Length;
            } else {
                index = currentIndex % _orderedPlayersArray.Length;
            }

            neighboor = _orderedPlayersArray[index];
            currentIndex += deltaIndex;
        }

        return neighboor;
    }

    private void animatePlayerHit(PlayerInfo playerInfo, int deltaIndex)
    {
        Animator playerAnimation = playerInfo.playerObject.GetComponentInChildren<Animator>();

        if (deltaIndex == 1) {
            playerAnimation.Play("HitRight");
        } else if (deltaIndex == -1) {
            playerAnimation.Play("HitLeft");
        }
    }

    public PlayerInfo getPlayerInfo(int deviceId)
    {
        for (int i = 0; i < _orderedPlayersArray.Length; i++)
        {
            if (_orderedPlayersArray[i] == null) {
                continue;
            } else if (_orderedPlayersArray[i].deviceId == deviceId) {
                return _orderedPlayersArray[i];
            }
        }

        return null;
    }

    public int getPlayerSpawnedPosition(PlayerInfo playerInfo)
    {
        for (int i = 0; i < _playersInSpawnersArray.Length; i++) {
            if (_playersInSpawnersArray[i].deviceId == playerInfo.deviceId) {
                return i;
            }
        }

        return -1;
    }

    public int getPlayerOrderedPosition(PlayerInfo playerInfo)
    {
        for (int i = 0; i < _orderedPlayersArray.Length; i++)
        {
            if (_orderedPlayersArray[i] == null) {
                continue;
            }

            if (_orderedPlayersArray[i].deviceId == playerInfo.deviceId)
            {
                return i;
            }
        }

        return -1;
    }

    public int getConnectedPlayers()
    {
        return connectedPlayers;
    }

    public PlayerInfo[] getOrderedPlayersArray()
    {
        return _orderedPlayersArray;
    }

    public PlayerInfo checkForWinner()
    {
        int bestScore = -1;
        PlayerInfo winner = null;

        for (int i = 0; i < _orderedPlayersArray.Length; i++) {
            if (_orderedPlayersArray[i] == null) {
                continue;
            }

            if (_orderedPlayersArray[i].getScore() > bestScore) {
                bestScore = _orderedPlayersArray[i].getScore();
                winner = _orderedPlayersArray[i];
            }
            _orderedPlayersArray[i].resetScore();
        }

        return winner;
    }
}
