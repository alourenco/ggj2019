﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public Canvas mainMenu;
    public Canvas gameHUD;
    public Canvas gameStartCounterHUD;
    public Canvas resultHUD;
    public Canvas endMenu;
    public Text timerText;
    public Text gameStartCounterText;
    public Text playerNameText;
    public Text roundText;
    public Text endPlayerName;
    public Text[] scoreText;
    public Text[] winsText;

    private void Start()
    {
        gameHUD.enabled = false;
        gameStartCounterHUD.enabled = false;
        resultHUD.enabled = false;
        endMenu.enabled = false;
    }

    public void closeMainMenu()
    {
        mainMenu.enabled = false;
        gameStartCounterHUD.enabled = true;
    }

    public void openMainMenu()
    {
        mainMenu.enabled = true;
        gameHUD.enabled = false;
    }
    
    public void hideGameStartCounter()
    {
        gameStartCounterHUD.enabled = false;
        gameHUD.enabled = true;
        resultHUD.enabled = false;
    }

    public void updateScore(int index, PlayerManager.PlayerInfo playerInfo)
    {
        string playerName = playerInfo.getPlayerName();
        int score = playerInfo.getScore();
        scoreText[index].text = playerName + ": " + score;
    }

    public void updateWins(int index, int wins)
    {
        winsText[index].text = "Wins: " + wins;
    }

    public void updateRoundTimer(int seconds)
    {
        string prefix = "";
        if (seconds < 10) {
            prefix = "0";
        }

        timerText.text = prefix + seconds + "s";
    }

    public void updateGameStartCounter(int seconds)
    {
        gameStartCounterText.text = seconds + "s";
    }

    public void updateCurrentRound(int round)
    {
        roundText.text = "Round: " + round;
    }

    public void updateAllScores(PlayerManager playerManager)
    {
        var orderedPlayers = playerManager.getOrderedPlayersArray();
        for (int i = 0; i < orderedPlayers.Length; i++) {
            if (orderedPlayers[i] != null) {
                updateScore(playerManager.getPlayerSpawnedPosition(orderedPlayers[i]), orderedPlayers[i]);
            }
        }
    }

    public void showResultHUD(string playerName)
    {
        resultHUD.enabled = true;
        playerNameText.text = playerName;
    }

    public void showEndMenu(string winnerName)
    {
        endMenu.enabled = true;
        endPlayerName.text = winnerName;
    }

    public void enableScore(int index, PlayerManager.PlayerInfo playerInfo)
    {
        string playerName = playerInfo.getPlayerName();
        int score = playerInfo.getScore();
        scoreText[index].text = playerName + ": " + score;
    }
}
