﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addScore() {
        score += 10;
    }

    public void removeScore() {
        score -= 2;
    }

    public int getScore() {
        return score;
    }
}
