﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFXManager : MonoBehaviour
{
    public AudioClip hitSound;

    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void playHitSound()
    {
        _audioSource.clip = hitSound;
        _audioSource.Play();
    }

}
