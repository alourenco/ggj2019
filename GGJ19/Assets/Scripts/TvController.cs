﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TvController : MonoBehaviour
{
    public Material onMaterial;
    public Material offMaterial;
    public Material redMaterial;
    public Material greenMaterial;
    public Material blueMaterial;
    public Material yellowMaterial;

    public Renderer screenPlane;

    public float colorShiftSeconds = 1.0F;
    public float colorShiftInbtermediateSeconds = 0.5F;
    public float colorShiftRandomThreshold = 0.5F;

    private string _currentColor = ProtoConstants.black;
    private bool _isTvOn = false;

    private float _elapsedTime = 0.0F;
    private float _secondsToNextShift = 0.0F;
    private string[] _tvColors;
    private Hashtable _colorMaterialMap;
    private bool _isInIntermediateStage = false;
    private float _currentColorShiftRate = 0.0F;

    private void Start()
    {
        _currentColorShiftRate = 0.0F;

        _tvColors = new string[] {
            ProtoConstants.red,
            ProtoConstants.green,
            ProtoConstants.blue,
            ProtoConstants.yellow
        };

        _colorMaterialMap = new Hashtable {
            {ProtoConstants.red, redMaterial},
            {ProtoConstants.green, greenMaterial},
            {ProtoConstants.blue, blueMaterial},
            {ProtoConstants.yellow, yellowMaterial}
        };
    }

    void Update()
    {
        if (!_isTvOn) {
            return;
        }

        if (_elapsedTime > _secondsToNextShift) {
            updateTvColor();
        } else {
            _elapsedTime += Time.deltaTime;
        }
    }

    private void updateTvColor()
    {
        if (_isInIntermediateStage) {
            _currentColor = getRandomColor();
            screenPlane.material = (Material)_colorMaterialMap[_currentColor];
            _secondsToNextShift = calcRandomShiftRate();
            _isInIntermediateStage = false;
        } else {
            _currentColor = ProtoConstants.white;
            screenPlane.material = onMaterial;
            _secondsToNextShift = colorShiftInbtermediateSeconds;
            _isInIntermediateStage = true;
        }

        _elapsedTime = 0.0F;
    }

    private float calcRandomShiftRate()
    {
        return Random.Range(colorShiftSeconds - colorShiftRandomThreshold, 
            colorShiftSeconds + colorShiftRandomThreshold);
    }

    private string getRandomColor()
    {
        string randomColor = _currentColor;
        while (randomColor.Equals(_currentColor)) {
            randomColor = _tvColors[Random.Range(0, _tvColors.Length)];
        }

        return randomColor;
    }

    public string getCurrentColor()
    {
        if (isTvColored()) {
            return _currentColor;
        }

        return "";
    }

    private bool isTvColored()
    {
        return !(_currentColor.Equals(ProtoConstants.white) ||
            _currentColor.Equals(ProtoConstants.black));
    }

    public void turnOff()
    {
        _isTvOn = false;
        screenPlane.material = offMaterial;
    }

    public void turnOn()
    {
        _isTvOn = true;
        screenPlane.material = onMaterial;
        _elapsedTime = 0.0F;
    }

    public bool isTvOn()
    {
        return _isTvOn;
    }
}
